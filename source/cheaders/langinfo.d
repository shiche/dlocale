/** 
   A reduced copy for langinfo.h from GNU. See copyright on it below.

   Access to locale-dependent parameters.
   Copyright (C) 1995-2020 Free Software Foundation, Inc.
   This file is part of the GNU C Library.

   The GNU C Library is free software; you can redistribute it and/or
   modify it under the terms of the GNU Lesser General Public
   License as published by the Free Software Foundation; either
   version 2.1 of the License, or (at your option) any later version.

   The GNU C Library is distributed in the hope that it will be useful,
   but WITHOUT ANY WARRANTY; without even the implied warranty of
   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
   Lesser General Public License for more details.

   You should have received a copy of the GNU Lesser General Public
   License along with the GNU C Library; if not, see
   <https://www.gnu.org/licenses/>.  */
module cheaders.langinfo;

version(Posix)
{
	extern (C):
	
	enum _LANGINFO_H = 1;
    auto _NL_ITEM(T0, T1)(auto ref T0 category, auto ref T1 index)
    {
        return (category << 16) | index;
    }
	
	/* Get the type definition.  */
	enum __LC_CTYPE = 0;
	enum __LC_NUMERIC = 1;
	enum __LC_TIME = 2;
	enum __LC_COLLATE = 3;
	enum __LC_MONETARY = 4;
	enum __LC_MESSAGES = 5;
	enum __LC_ALL = 6;
	enum __LC_PAPER = 7;
	enum __LC_NAME = 8;
	enum __LC_ADDRESS = 9;
	enum __LC_TELEPHONE = 10;
	enum __LC_MEASUREMENT = 11;
	enum __LC_IDENTIFICATION = 12;	

	/* Enumeration of locale items that can be queried with `nl_langinfo'.  */
	enum nl_item
	{
	    /* LC_TIME category: date and time formatting.  */
	
	    /* Abbreviated days of the week. */
	    ABDAY_1 = _NL_ITEM(__LC_TIME, 0), /* Sun */
	
	    ABDAY_2 = 131073,
	
	    ABDAY_3 = 131074,
	
	    ABDAY_4 = 131075,
	
	    ABDAY_5 = 131076,
	
	    ABDAY_6 = 131077,
	
	    ABDAY_7 = 131078,
	
	    /* Long-named days of the week. */
	    DAY_1 = 131079, /* Sunday */
	
	    DAY_2 = 131080, /* Monday */
	
	    DAY_3 = 131081, /* Tuesday */
	
	    DAY_4 = 131082, /* Wednesday */
	
	    DAY_5 = 131083, /* Thursday */
	
	    DAY_6 = 131084, /* Friday */
	
	    DAY_7 = 131085, /* Saturday */
	
	    /* Abbreviated month names, in the grammatical form used when the month
	       is a part of a complete date.  */
	    ABMON_1 = 131086, /* Jan */
	
	    ABMON_2 = 131087,
	
	    ABMON_3 = 131088,
	
	    ABMON_4 = 131089,
	
	    ABMON_5 = 131090,
	
	    ABMON_6 = 131091,
	
	    ABMON_7 = 131092,
	
	    ABMON_8 = 131093,
	
	    ABMON_9 = 131094,
	
	    ABMON_10 = 131095,
	
	    ABMON_11 = 131096,
	
	    ABMON_12 = 131097,
	
	    /* Long month names, in the grammatical form used when the month
	       is a part of a complete date.  */
	    MON_1 = 131098, /* January */
	
	    MON_2 = 131099,
	
	    MON_3 = 131100,
	
	    MON_4 = 131101,
	
	    MON_5 = 131102,
	
	    MON_6 = 131103,
	
	    MON_7 = 131104,
	
	    MON_8 = 131105,
	
	    MON_9 = 131106,
	
	    MON_10 = 131107,
	
	    MON_11 = 131108,
	
	    MON_12 = 131109,
	
	    AM_STR = 131110, /* Ante meridiem string.  */
	
	    PM_STR = 131111, /* Post meridiem string.  */
	
	    D_T_FMT = 131112, /* Date and time format for strftime.  */
	
	    D_FMT = 131113, /* Date format for strftime.  */
	
	    T_FMT = 131114, /* Time format for strftime.  */
	
	    T_FMT_AMPM = 131115, /* 12-hour time format for strftime.  */
	
	    ERA = 131116, /* Alternate era.  */
	
	    __ERA_YEAR = 131117, /* Year in alternate era format.  */
	
	    ERA_D_FMT = 131118, /* Date in alternate era format.  */
	
	    ALT_DIGITS = 131119, /* Alternate symbols for digits.  */
	
	    ERA_D_T_FMT = 131120, /* Date and time in alternate era format.  */
	
	    ERA_T_FMT = 131121, /* Time in alternate era format.  */
	
	    _NL_TIME_ERA_NUM_ENTRIES = 131122, /* Number entries in the era arrays.  */
	    _NL_TIME_ERA_ENTRIES = 131123, /* Structure with era entries in usable form.*/
	
	    _NL_WABDAY_1 = 131124, /* Sun */
	    _NL_WABDAY_2 = 131125,
	    _NL_WABDAY_3 = 131126,
	    _NL_WABDAY_4 = 131127,
	    _NL_WABDAY_5 = 131128,
	    _NL_WABDAY_6 = 131129,
	    _NL_WABDAY_7 = 131130,
	
	    /* Long-named days of the week. */
	    _NL_WDAY_1 = 131131, /* Sunday */
	    _NL_WDAY_2 = 131132, /* Monday */
	    _NL_WDAY_3 = 131133, /* Tuesday */
	    _NL_WDAY_4 = 131134, /* Wednesday */
	    _NL_WDAY_5 = 131135, /* Thursday */
	    _NL_WDAY_6 = 131136, /* Friday */
	    _NL_WDAY_7 = 131137, /* Saturday */
	
	    /* Abbreviated month names, in the grammatical form used when the month
	       is a part of a complete date.  */
	    _NL_WABMON_1 = 131138, /* Jan */
	    _NL_WABMON_2 = 131139,
	    _NL_WABMON_3 = 131140,
	    _NL_WABMON_4 = 131141,
	    _NL_WABMON_5 = 131142,
	    _NL_WABMON_6 = 131143,
	    _NL_WABMON_7 = 131144,
	    _NL_WABMON_8 = 131145,
	    _NL_WABMON_9 = 131146,
	    _NL_WABMON_10 = 131147,
	    _NL_WABMON_11 = 131148,
	    _NL_WABMON_12 = 131149,
	
	    /* Long month names, in the grammatical form used when the month
	       is a part of a complete date.  */
	    _NL_WMON_1 = 131150, /* January */
	    _NL_WMON_2 = 131151,
	    _NL_WMON_3 = 131152,
	    _NL_WMON_4 = 131153,
	    _NL_WMON_5 = 131154,
	    _NL_WMON_6 = 131155,
	    _NL_WMON_7 = 131156,
	    _NL_WMON_8 = 131157,
	    _NL_WMON_9 = 131158,
	    _NL_WMON_10 = 131159,
	    _NL_WMON_11 = 131160,
	    _NL_WMON_12 = 131161,
	
	    _NL_WAM_STR = 131162, /* Ante meridiem string.  */
	    _NL_WPM_STR = 131163, /* Post meridiem string.  */
	
	    _NL_WD_T_FMT = 131164, /* Date and time format for strftime.  */
	    _NL_WD_FMT = 131165, /* Date format for strftime.  */
	    _NL_WT_FMT = 131166, /* Time format for strftime.  */
	    _NL_WT_FMT_AMPM = 131167, /* 12-hour time format for strftime.  */
	
	    _NL_WERA_YEAR = 131168, /* Year in alternate era format.  */
	    _NL_WERA_D_FMT = 131169, /* Date in alternate era format.  */
	    _NL_WALT_DIGITS = 131170, /* Alternate symbols for digits.  */
	    _NL_WERA_D_T_FMT = 131171, /* Date and time in alternate era format.  */
	    _NL_WERA_T_FMT = 131172, /* Time in alternate era format.  */
	
	    _NL_TIME_WEEK_NDAYS = 131173,
	    _NL_TIME_WEEK_1STDAY = 131174,
	    _NL_TIME_WEEK_1STWEEK = 131175,
	    _NL_TIME_FIRST_WEEKDAY = 131176,
	    _NL_TIME_FIRST_WORKDAY = 131177,
	    _NL_TIME_CAL_DIRECTION = 131178,
	    _NL_TIME_TIMEZONE = 131179,

	    /// strftime format for date.
	    _DATE_FMT = 131180, 	
	    _NL_W_DATE_FMT = 131181,
	
	    _NL_TIME_CODESET = 131182,
	
	    /// Long month names, in the grammatical form used when the month is named by itself.
	    __ALTMON_1 = 131183, // January
	    __ALTMON_2 = 131184,
	    __ALTMON_3 = 131185,
	    __ALTMON_4 = 131186,
	    __ALTMON_5 = 131187,
	    __ALTMON_6 = 131188,
	    __ALTMON_7 = 131189,
	    __ALTMON_8 = 131190,
	    __ALTMON_9 = 131191,
	    __ALTMON_10 = 131192,
	    __ALTMON_11 = 131193,
	    __ALTMON_12 = 131194,
	
	    /// Long month names, in the grammatical form used when the month is named by itself.
	    _NL_WALTMON_1 = 131195, // January
	    _NL_WALTMON_2 = 131196,
	    _NL_WALTMON_3 = 131197,
	    _NL_WALTMON_4 = 131198,
	    _NL_WALTMON_5 = 131199,
	    _NL_WALTMON_6 = 131200,
	    _NL_WALTMON_7 = 131201,
	    _NL_WALTMON_8 = 131202,
	    _NL_WALTMON_9 = 131203,
	    _NL_WALTMON_10 = 131204,
	    _NL_WALTMON_11 = 131205,
	    _NL_WALTMON_12 = 131206,
	
	    /// Abbreviated month names, in the grammatical form used when the month is named by itself.
	    _NL_ABALTMON_1 = 131207, // Jan
	    _NL_ABALTMON_2 = 131208,
	    _NL_ABALTMON_3 = 131209,
	    _NL_ABALTMON_4 = 131210,
	    _NL_ABALTMON_5 = 131211,
	    _NL_ABALTMON_6 = 131212,
	    _NL_ABALTMON_7 = 131213,
	    _NL_ABALTMON_8 = 131214,
	    _NL_ABALTMON_9 = 131215,
	    _NL_ABALTMON_10 = 131216,
	    _NL_ABALTMON_11 = 131217,
	    _NL_ABALTMON_12 = 131218,
	
	    /** Abbreviated month names, in the grammatical form used when the month
	       is named by itself.  */
	    _NL_WABALTMON_1 = 131219, // Jan
	    _NL_WABALTMON_2 = 131220,
	    _NL_WABALTMON_3 = 131221,
	    _NL_WABALTMON_4 = 131222,
	    _NL_WABALTMON_5 = 131223,
	    _NL_WABALTMON_6 = 131224,
	    _NL_WABALTMON_7 = 131225,
	    _NL_WABALTMON_8 = 131226,
	    _NL_WABALTMON_9 = 131227,
	    _NL_WABALTMON_10 = 131228,
	    _NL_WABALTMON_11 = 131229,
	    _NL_WABALTMON_12 = 131230,

	    /** Number of indices in LC_TIME category.  */
	    _NL_NUM_LC_TIME = 131231, 	

	    /** LC_COLLATE category: text sorting.
	       This information is accessed by the strcoll and strxfrm functions.
	       These `nl_langinfo' names are used only internally.  */
	    _NL_COLLATE_NRULES = _NL_ITEM(__LC_COLLATE, 0),
	    _NL_COLLATE_RULESETS = 196609,
	    _NL_COLLATE_TABLEMB = 196610,
	    _NL_COLLATE_WEIGHTMB = 196611,
	    _NL_COLLATE_EXTRAMB = 196612,
	    _NL_COLLATE_INDIRECTMB = 196613,
	    _NL_COLLATE_GAP1 = 196614,
	    _NL_COLLATE_GAP2 = 196615,
	    _NL_COLLATE_GAP3 = 196616,
	    _NL_COLLATE_TABLEWC = 196617,
	    _NL_COLLATE_WEIGHTWC = 196618,
	    _NL_COLLATE_EXTRAWC = 196619,
	    _NL_COLLATE_INDIRECTWC = 196620,
	    _NL_COLLATE_SYMB_HASH_SIZEMB = 196621,
	    _NL_COLLATE_SYMB_TABLEMB = 196622,
	    _NL_COLLATE_SYMB_EXTRAMB = 196623,
	    _NL_COLLATE_COLLSEQMB = 196624,
	    _NL_COLLATE_COLLSEQWC = 196625,
	    _NL_COLLATE_CODESET = 196626,
	    _NL_NUM_LC_COLLATE = 196627,
	
	    /** LC_CTYPE category: character classification.
	       This information is accessed by the functions in <ctype.h>.
	       These `nl_langinfo' names are used only internally.  */
	    _NL_CTYPE_CLASS = _NL_ITEM(__LC_CTYPE, 0),
	    _NL_CTYPE_TOUPPER = 1,
	    _NL_CTYPE_GAP1 = 2,
	    _NL_CTYPE_TOLOWER = 3,
	    _NL_CTYPE_GAP2 = 4,
	    _NL_CTYPE_CLASS32 = 5,
	    _NL_CTYPE_GAP3 = 6,
	    _NL_CTYPE_GAP4 = 7,
	    _NL_CTYPE_GAP5 = 8,
	    _NL_CTYPE_GAP6 = 9,
	    _NL_CTYPE_CLASS_NAMES = 10,
	    _NL_CTYPE_MAP_NAMES = 11,
	    _NL_CTYPE_WIDTH = 12,
	    _NL_CTYPE_MB_CUR_MAX = 13,
	    _NL_CTYPE_CODESET_NAME = 14,
	    CODESET = _NL_CTYPE_CODESET_NAME,
	
	    _NL_CTYPE_TOUPPER32 = 15,
	    _NL_CTYPE_TOLOWER32 = 16,
	    _NL_CTYPE_CLASS_OFFSET = 17,
	    _NL_CTYPE_MAP_OFFSET = 18,
	    _NL_CTYPE_INDIGITS_MB_LEN = 19,
	    _NL_CTYPE_INDIGITS0_MB = 20,
	    _NL_CTYPE_INDIGITS1_MB = 21,
	    _NL_CTYPE_INDIGITS2_MB = 22,
	    _NL_CTYPE_INDIGITS3_MB = 23,
	    _NL_CTYPE_INDIGITS4_MB = 24,
	    _NL_CTYPE_INDIGITS5_MB = 25,
	    _NL_CTYPE_INDIGITS6_MB = 26,
	    _NL_CTYPE_INDIGITS7_MB = 27,
	    _NL_CTYPE_INDIGITS8_MB = 28,
	    _NL_CTYPE_INDIGITS9_MB = 29,
	    _NL_CTYPE_INDIGITS_WC_LEN = 30,
	    _NL_CTYPE_INDIGITS0_WC = 31,
	    _NL_CTYPE_INDIGITS1_WC = 32,
	    _NL_CTYPE_INDIGITS2_WC = 33,
	    _NL_CTYPE_INDIGITS3_WC = 34,
	    _NL_CTYPE_INDIGITS4_WC = 35,
	    _NL_CTYPE_INDIGITS5_WC = 36,
	    _NL_CTYPE_INDIGITS6_WC = 37,
	    _NL_CTYPE_INDIGITS7_WC = 38,
	    _NL_CTYPE_INDIGITS8_WC = 39,
	    _NL_CTYPE_INDIGITS9_WC = 40,
	    _NL_CTYPE_OUTDIGIT0_MB = 41,
	    _NL_CTYPE_OUTDIGIT1_MB = 42,
	    _NL_CTYPE_OUTDIGIT2_MB = 43,
	    _NL_CTYPE_OUTDIGIT3_MB = 44,
	    _NL_CTYPE_OUTDIGIT4_MB = 45,
	    _NL_CTYPE_OUTDIGIT5_MB = 46,
	    _NL_CTYPE_OUTDIGIT6_MB = 47,
	    _NL_CTYPE_OUTDIGIT7_MB = 48,
	    _NL_CTYPE_OUTDIGIT8_MB = 49,
	    _NL_CTYPE_OUTDIGIT9_MB = 50,
	    _NL_CTYPE_OUTDIGIT0_WC = 51,
	    _NL_CTYPE_OUTDIGIT1_WC = 52,
	    _NL_CTYPE_OUTDIGIT2_WC = 53,
	    _NL_CTYPE_OUTDIGIT3_WC = 54,
	    _NL_CTYPE_OUTDIGIT4_WC = 55,
	    _NL_CTYPE_OUTDIGIT5_WC = 56,
	    _NL_CTYPE_OUTDIGIT6_WC = 57,
	    _NL_CTYPE_OUTDIGIT7_WC = 58,
	    _NL_CTYPE_OUTDIGIT8_WC = 59,
	    _NL_CTYPE_OUTDIGIT9_WC = 60,
	    _NL_CTYPE_TRANSLIT_TAB_SIZE = 61,
	    _NL_CTYPE_TRANSLIT_FROM_IDX = 62,
	    _NL_CTYPE_TRANSLIT_FROM_TBL = 63,
	    _NL_CTYPE_TRANSLIT_TO_IDX = 64,
	    _NL_CTYPE_TRANSLIT_TO_TBL = 65,
	    _NL_CTYPE_TRANSLIT_DEFAULT_MISSING_LEN = 66,
	    _NL_CTYPE_TRANSLIT_DEFAULT_MISSING = 67,
	    _NL_CTYPE_TRANSLIT_IGNORE_LEN = 68,
	    _NL_CTYPE_TRANSLIT_IGNORE = 69,
	    _NL_CTYPE_MAP_TO_NONASCII = 70,
	    _NL_CTYPE_NONASCII_CASE = 71,
	    _NL_CTYPE_EXTRA_MAP_1 = 72,
	    _NL_CTYPE_EXTRA_MAP_2 = 73,
	    _NL_CTYPE_EXTRA_MAP_3 = 74,
	    _NL_CTYPE_EXTRA_MAP_4 = 75,
	    _NL_CTYPE_EXTRA_MAP_5 = 76,
	    _NL_CTYPE_EXTRA_MAP_6 = 77,
	    _NL_CTYPE_EXTRA_MAP_7 = 78,
	    _NL_CTYPE_EXTRA_MAP_8 = 79,
	    _NL_CTYPE_EXTRA_MAP_9 = 80,
	    _NL_CTYPE_EXTRA_MAP_10 = 81,
	    _NL_CTYPE_EXTRA_MAP_11 = 82,
	    _NL_CTYPE_EXTRA_MAP_12 = 83,
	    _NL_CTYPE_EXTRA_MAP_13 = 84,
	    _NL_CTYPE_EXTRA_MAP_14 = 85,
	    _NL_NUM_LC_CTYPE = 86,
	
	    /** LC_MONETARY category: formatting of monetary quantities.
	       These items each correspond to a member of `struct lconv',
	       defined in <locale.h>.  */
	    __INT_CURR_SYMBOL = _NL_ITEM(__LC_MONETARY, 0),
	
	    __CURRENCY_SYMBOL = 262145,
	
	    __MON_DECIMAL_POINT = 262146,
	
	    __MON_THOUSANDS_SEP = 262147,
	
	    __MON_GROUPING = 262148,
	
	    __POSITIVE_SIGN = 262149,
	
	    __NEGATIVE_SIGN = 262150,
	
	    __INT_FRAC_DIGITS = 262151,
	
	    __FRAC_DIGITS = 262152,
	
	    __P_CS_PRECEDES = 262153,
	
	    __P_SEP_BY_SPACE = 262154,
	
	    __N_CS_PRECEDES = 262155,
	
	    __N_SEP_BY_SPACE = 262156,
	
	    __P_SIGN_POSN = 262157,
	
	    __N_SIGN_POSN = 262158,
	
	    _NL_MONETARY_CRNCYSTR = 262159,
	
	    __INT_P_CS_PRECEDES = 262160,
	
	    __INT_P_SEP_BY_SPACE = 262161,
	
	    __INT_N_CS_PRECEDES = 262162,
	
	    __INT_N_SEP_BY_SPACE = 262163,
	
	    __INT_P_SIGN_POSN = 262164,
	
	    __INT_N_SIGN_POSN = 262165,
	
	    _NL_MONETARY_DUO_INT_CURR_SYMBOL = 262166,
	    _NL_MONETARY_DUO_CURRENCY_SYMBOL = 262167,
	    _NL_MONETARY_DUO_INT_FRAC_DIGITS = 262168,
	    _NL_MONETARY_DUO_FRAC_DIGITS = 262169,
	    _NL_MONETARY_DUO_P_CS_PRECEDES = 262170,
	    _NL_MONETARY_DUO_P_SEP_BY_SPACE = 262171,
	    _NL_MONETARY_DUO_N_CS_PRECEDES = 262172,
	    _NL_MONETARY_DUO_N_SEP_BY_SPACE = 262173,
	    _NL_MONETARY_DUO_INT_P_CS_PRECEDES = 262174,
	    _NL_MONETARY_DUO_INT_P_SEP_BY_SPACE = 262175,
	    _NL_MONETARY_DUO_INT_N_CS_PRECEDES = 262176,
	    _NL_MONETARY_DUO_INT_N_SEP_BY_SPACE = 262177,
	    _NL_MONETARY_DUO_P_SIGN_POSN = 262178,
	    _NL_MONETARY_DUO_N_SIGN_POSN = 262179,
	    _NL_MONETARY_DUO_INT_P_SIGN_POSN = 262180,
	    _NL_MONETARY_DUO_INT_N_SIGN_POSN = 262181,
	    _NL_MONETARY_UNO_VALID_FROM = 262182,
	    _NL_MONETARY_UNO_VALID_TO = 262183,
	    _NL_MONETARY_DUO_VALID_FROM = 262184,
	    _NL_MONETARY_DUO_VALID_TO = 262185,
	    _NL_MONETARY_CONVERSION_RATE = 262186,
	    _NL_MONETARY_DECIMAL_POINT_WC = 262187,
	    _NL_MONETARY_THOUSANDS_SEP_WC = 262188,
	    _NL_MONETARY_CODESET = 262189,
	    _NL_NUM_LC_MONETARY = 262190,
	
	    /// LC_NUMERIC category: formatting of numbers. These also correspond to members of `struct lconv'; see <locale.h>.
	    __DECIMAL_POINT = _NL_ITEM(__LC_NUMERIC, 0),
	
	    RADIXCHAR = __DECIMAL_POINT,
	
	    __THOUSANDS_SEP = 65537,
	
	    THOUSEP = __THOUSANDS_SEP,
	
	    __GROUPING = 65538,
	
	    _NL_NUMERIC_DECIMAL_POINT_WC = 65539,
	    _NL_NUMERIC_THOUSANDS_SEP_WC = 65540,
	    _NL_NUMERIC_CODESET = 65541,
	    _NL_NUM_LC_NUMERIC = 65542,
	    
        /// Regex matching ``yes'' input.
	    __YESEXPR = _NL_ITEM(__LC_MESSAGES, 0), 
	
        /// Regex matching ``no'' input.
	    __NOEXPR = 327681,
	    /// Output string for ``yes''.
	    __YESSTR = 327682, 

	    /// Output string for ``no''.
	    __NOSTR = 327683, 	
	    _NL_MESSAGES_CODESET = 327684,
	    _NL_NUM_LC_MESSAGES = 327685,
	
	    _NL_PAPER_HEIGHT = _NL_ITEM(__LC_PAPER, 0),
	    _NL_PAPER_WIDTH = 458753,
	    _NL_PAPER_CODESET = 458754,
	    _NL_NUM_LC_PAPER = 458755,
	
	    _NL_NAME_NAME_FMT = _NL_ITEM(__LC_NAME, 0),
	    _NL_NAME_NAME_GEN = 524289,
	    _NL_NAME_NAME_MR = 524290,
	    _NL_NAME_NAME_MRS = 524291,
	    _NL_NAME_NAME_MISS = 524292,
	    _NL_NAME_NAME_MS = 524293,
	    _NL_NAME_CODESET = 524294,
	    _NL_NUM_LC_NAME = 524295,
	
	    _NL_ADDRESS_POSTAL_FMT = _NL_ITEM(__LC_ADDRESS, 0),
	    _NL_ADDRESS_COUNTRY_NAME = 589825,
	    _NL_ADDRESS_COUNTRY_POST = 589826,
	    _NL_ADDRESS_COUNTRY_AB2 = 589827,
	    _NL_ADDRESS_COUNTRY_AB3 = 589828,
	    _NL_ADDRESS_COUNTRY_CAR = 589829,
	    _NL_ADDRESS_COUNTRY_NUM = 589830,
	    _NL_ADDRESS_COUNTRY_ISBN = 589831,
	    _NL_ADDRESS_LANG_NAME = 589832,
	    _NL_ADDRESS_LANG_AB = 589833,
	    _NL_ADDRESS_LANG_TERM = 589834,
	    _NL_ADDRESS_LANG_LIB = 589835,
	    _NL_ADDRESS_CODESET = 589836,
	    _NL_NUM_LC_ADDRESS = 589837,
	
	    _NL_TELEPHONE_TEL_INT_FMT = _NL_ITEM(__LC_TELEPHONE, 0),
	    _NL_TELEPHONE_TEL_DOM_FMT = 655361,
	    _NL_TELEPHONE_INT_SELECT = 655362,
	    _NL_TELEPHONE_INT_PREFIX = 655363,
	    _NL_TELEPHONE_CODESET = 655364,
	    _NL_NUM_LC_TELEPHONE = 655365,
	
	    _NL_MEASUREMENT_MEASUREMENT = _NL_ITEM(__LC_MEASUREMENT, 0),
	    _NL_MEASUREMENT_CODESET = 720897,
	    _NL_NUM_LC_MEASUREMENT = 720898,
	
	    _NL_IDENTIFICATION_TITLE = _NL_ITEM(__LC_IDENTIFICATION, 0),
	    _NL_IDENTIFICATION_SOURCE = 786433,
	    _NL_IDENTIFICATION_ADDRESS = 786434,
	    _NL_IDENTIFICATION_CONTACT = 786435,
	    _NL_IDENTIFICATION_EMAIL = 786436,
	    _NL_IDENTIFICATION_TEL = 786437,
	    _NL_IDENTIFICATION_FAX = 786438,
	    _NL_IDENTIFICATION_LANGUAGE = 786439,
	    _NL_IDENTIFICATION_TERRITORY = 786440,
	    _NL_IDENTIFICATION_AUDIENCE = 786441,
	    _NL_IDENTIFICATION_APPLICATION = 786442,
	    _NL_IDENTIFICATION_ABBREVIATION = 786443,
	    _NL_IDENTIFICATION_REVISION = 786444,
	    _NL_IDENTIFICATION_DATE = 786445,
	    _NL_IDENTIFICATION_CATEGORY = 786446,
	    _NL_IDENTIFICATION_CODESET = 786447,
	    _NL_NUM_LC_IDENTIFICATION = 786448,
	
	    /// This marks the highest value used.
	    _NL_NUM = 786449
	}
	
	/** Return the current locale's value for ITEM.
	   If ITEM is invalid, an empty string is returned.
	
	   The string returned will not change until `setlocale' is called;
	   it is usually in read-only memory and cannot be modified.  */
	char* nl_langinfo (nl_item __item);
	
}
