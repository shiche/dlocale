/**
* Locale information routines.
*/
module dlocale;

import std.datetime;
import core.stdc.time;
import core.stdc.locale;
import std.conv;
import std.string;
import std.process : environment;
import std.math : abs;

version(Windows) {
    import core.sys.windows.windows;
    import core.sys.windows.winnls;
    import std.algorithm : min;
	import std.string : strip;
}

/// Info about locale.
struct Locale
{
    /// Locale name. For example, ru-RU.
    string name;

    /// Code set
    string codeSet;

    // Date/time related block

    /// Era
    string era;
    /// Full week's day name, starting with Sunday.
    string[7] weekDays;
    /// Abbreviated week's day name, starting with Sun.
    string[7] abbrWeekDays;

    /// Full month name.
    string[12] monthes;
    /// Abbreviated month name.
    string[12] abbrMonthes;

    /// AM and PM values, if any
    string am, pm;
    /// String that can be used as a format string to represent time and date in a locale-specific way.
    string dateTime;
    /// String that can be used as a format string to represent date in a locale-specific way.
    string date;
    /// String that can be used as a format string to represent time in a locale-specific way.
    string time;

    // Numeric related block

    /// Decimal point character (decimal dot, decimal comma, etc.).
    string decimalPoint;
    /// Separator character for thousands (groups of three digits).
    string thousandSeparator;
    
    // Monetary

    /// Currency symbol in native format.
    string currency;
    /// Currency symbol in international format.
    string intCurrency;
    /// Currency native place for positive amounts 1 - symbol precedes value, 0 - follows it.
    byte currencyPositivePlace;
    /// Currency native place for negative amounts 1 - symbol precedes value, 0 - follows it.
    byte currencyNegativePlace;
    /// Currency international place for positive amounts 1 - symbol precedes value, 0 - follows it.
    byte intCurrencyPositivePlace;
    /// Currency international place for negative amounts 1 - symbol precedes value, 0 - follows it.
    byte intCurrencyNegativePlace;
    /// Native digits after decimal place for money.
    byte fracDigits;
    /// International digits after decimal place for money.
    byte intFracDigits;

    string toString() { return name; }
}

/**
* Sets default locale for library. Automatically loads locale info for future use.
* Params:
*   localeName = Locale system name. For example, C.UTF-8 or u_RU.UTF-8.
*/
export void setDefaultLocale(string localeName)
{
    defaultLocale = initDateformatLocale(localeName);
}

/**
* Gets defaul locale for system.
* Returns:
*   Default locale.
*/
export Locale getDefaultLocale()
{
    return defaultLocale;
}

/**
* Locale for library.
* Params:
*   localeName = Locale system name. For example, C.UTF-8 or Ru_RU.UTF-8.
* Returns: Locale object with locale info.
*/
export Locale initDateformatLocale(string localeName)
{
    // Need collect information or it already collected?
    auto plocale = localeName in locales;
    if (plocale != null)
        return *plocale;

    Locale locale;
    locale.name = localeName;

    // Setting current locale for C routines.
    setlocale(LC_ALL, localeName.toStringz);
	
    auto moment = DateTime(2021, 1, 3, 0, 0, 0);
    
    // AM and PM words
	locale.am = _format(moment, "%p");
	moment += hours(15);
	locale.pm = _format(moment, "%p");

    version(Posix)
    {
        import cheaders.langinfo;

        /// Get item from locale definition.
        string discover(nl_item what)
        {
            return to!string(nl_langinfo(what));
        }

        /// Fills locale info into array
        void fillArray(string[] array, nl_item start, nl_item end)
        {
            for (auto item = start; item <= end; item++)
            {
                auto index = item - start;
                array[index] = discover(item);
            }
        }

        locale.codeSet = discover(nl_item.CODESET);
        
        // Date, time and both locale specific formats.
        locale.era = discover(nl_item.ERA);
        locale.dateTime = discover(nl_item.D_T_FMT);
        locale.date = discover(nl_item.D_FMT);
        locale.time = discover(nl_item.T_FMT);

        // Days of week
        fillArray(locale.weekDays, nl_item.DAY_1, nl_item.DAY_7);
        fillArray(locale.abbrWeekDays, nl_item.ABDAY_1, nl_item.ABDAY_7);
        
        // Monthes
        fillArray(locale.monthes, nl_item.MON_1, nl_item.MON_12);
        fillArray(locale.abbrMonthes, nl_item.ABMON_1, nl_item.ABMON_12);

        // Numeric
        locale.decimalPoint = discover(nl_item.__DECIMAL_POINT);
        locale.thousandSeparator = discover(nl_item.__THOUSANDS_SEP);

        // Currency
        locale.currency = discover(nl_item.__CURRENCY_SYMBOL).strip;
        locale.intCurrency = discover(nl_item.__INT_CURR_SYMBOL).strip;
        auto st = discover(nl_item.__N_CS_PRECEDES);
        locale.currencyPositivePlace = st.length > 0 ? abs(cast(byte)st[0]) : 0;         
        st = discover(nl_item.__P_CS_PRECEDES);
        locale.currencyNegativePlace = st.length > 0 ? abs(cast(byte)st[0]) : 0;         
        
        st = discover(nl_item.__INT_N_CS_PRECEDES);
        locale.intCurrencyPositivePlace = st.length > 0 ? abs(cast(byte)st[0]) : 0;         
        st = discover(nl_item.__INT_P_CS_PRECEDES);
        locale.intCurrencyNegativePlace = st.length > 0 ? abs(cast(byte)st[0]) : 0; 
        st = discover(nl_item.__INT_FRAC_DIGITS);
        auto b = cast(byte)st[0];
        locale.intFracDigits = st.length > 0 ? (b < 1 ? 0 : b) : 0; 
        st = discover(nl_item.__FRAC_DIGITS);
        b = cast(byte)st[0];
        locale.fracDigits = st.length > 0 ? (b < 1 ? 0 : b) : 0; 
    }
    else
        version(Windows)
	    {
	    	// Using Windows API to collect info
	    	auto LOCALE_SSHORTTIME = 0x00000079;

			LCID lcid = windowsLocales.get(localeName, LOCALE_USER_DEFAULT);

    		locale.date = getAndConvertFormat(lcid, LOCALE_SSHORTDATE);
	    	locale.time = getAndConvertFormat(lcid, LOCALE_SSHORTTIME);
			locale.dateTime = getAndConvertFormat(lcid, LOCALE_SLONGDATE)
				~ " "
				~ getAndConvertFormat(lcid, LOCALE_SSHORTTIME);

			// Week days
            for (auto wday = LOCALE_SDAYNAME1; wday < LOCALE_SDAYNAME7; wday++)
				locale.weekDays[wday - LOCALE_SDAYNAME1 + 1] = discover(lcid, wday);

			locale.weekDays[0] = discover(lcid, LOCALE_SDAYNAME7);
			
			for (auto wday = LOCALE_SABBREVDAYNAME1; wday < LOCALE_SABBREVDAYNAME7; wday++)
				locale.abbrWeekDays[wday - LOCALE_SABBREVDAYNAME1 + 1] = discover(lcid, wday);

			locale.abbrWeekDays[0] = discover(lcid, LOCALE_SABBREVDAYNAME7);
			
			// Monthes
			for (auto wmon = LOCALE_SMONTHNAME1; wmon <= LOCALE_SMONTHNAME12; wmon++)
				locale.monthes[wmon - LOCALE_SMONTHNAME1] = discover(lcid, wmon);

			for (auto wmon = LOCALE_SABBREVMONTHNAME1; wmon <= LOCALE_SABBREVMONTHNAME12; wmon++)
				locale.abbrMonthes[wmon - LOCALE_SABBREVMONTHNAME1] = discover(lcid, wmon);

            locale.codeSet = discover(lcid, LOCALE_IDEFAULTCODEPAGE);

            // TODO Locale ERA

            locale.decimalPoint = discover(lcid, LOCALE_SDECIMAL);
            locale.thousandSeparator = discover(lcid, LOCALE_STHOUSAND);
            locale.currency = discover(lcid, LOCALE_SCURRENCY);
            immutable auto posCurrency = to!int(discover(lcid, LOCALE_ICURRENCY));
            locale.currencyPositivePlace = (posCurrency == 0 || posCurrency == 2) ? 1 : 0;
            locale.currencyNegativePlace = (posCurrency == 0 || posCurrency == 2) ? 1 : 0;
            locale.fracDigits = to!byte(discover(lcid, LOCALE_ICURRDIGITS));

            locale.intCurrency = locale.currency;
            locale.intCurrencyPositivePlace = locale.currencyPositivePlace;
            locale.intCurrencyNegativePlace = locale.currencyNegativePlace;
            locale.intFracDigits = to!byte(discover(lcid, LOCALE_ICURRDIGITS));
	    }
        else // Unknown OS. Use standard D library to determine parameters.
        {
	        determineLocaleData(locale); 

            locale.dateTime = "%Y-%m-%dT%H:%M:%S";
            locale.date = "%Y-%m-%d";
            locale.time = "%H:%M:%S";
        }

    // Remember locale settings
    locales[localeName] = locale;

    return locale;
}

private immutable auto BUF_SIZE = 127;

/// Using standard C strftime for unknown systems.
private string _format(DateTime date, string formatString)
{
    char[] buf = new char[BUF_SIZE];
    char* p = &(buf[0]); 
    auto tm = (cast(SysTime)date).toTM;
    strftime(p, BUF_SIZE, formatString.toStringz, &tm);

    return to!string(fromStringz(p));
}

/// Determine some parameters for specified locale
private void determineLocaleData(Locale locale)
{
    auto moment = DateTime(2021, 1, 3, 0, 0, 0);
    do
	{
        locale.weekDays[moment.dayOfWeek] = _format(moment, "%A");
	    locale.abbrWeekDays[moment.dayOfWeek] = _format(moment, "%a");

	    moment += days(1);

    } while (moment.dayOfWeek != DayOfWeek.sun);

    do
	{
        auto curMonth = moment.month - 1;
	    locale.monthes[curMonth] = _format(moment, "%B");
	    locale.abbrMonthes[curMonth] =_format(moment, "%b");

	    moment.add!"months"(1);
    } while (moment.year == 2021);
}

/// Known locales
private Locale[string] locales;
/// Default locale. Autodetect language.
private Locale defaultLocale;
version(Windows)
{
    private LCID[string] windowsLocales;
}

/// Loading default locale.
static this()
{
    version(Posix)
    {
        setDefaultLocale(environment.get("LANG", "C"));
    }
    version(Windows)
    {
        windowsLocales = ["aa": 0x1000, "aa-DJ": 0x1000, "aa-ER": 0x1000, "aa-ET": 0x1000, "af": 0x0036, "af-NA": 0x1000, 
		"af-ZA": 0x0436, "agq": 0x1000, "agq-CM": 0x1000, "ak": 0x1000, "ak-GH": 0x1000, "sq": 0x001C, 
		"sq-AL": 0x041C, "sq-MK": 0x1000, "gsw": 0x0084, "gsw-FR": 0x0484, "gsw-LI": 0x1000, "gsw-CH": 0x1000, 
		"am": 0x005E, "am-ET": 0x045E, "ar": 0x0001, "ar-DZ": 0x1401, "ar-BH": 0x3C01, "ar-TD": 0x1000, 
		"ar-KM": 0x1000, "ar-DJ": 0x1000, "ar-EG": 0x0c01, "ar-ER": 0x1000, "ar-IQ": 0x0801, "ar-IL": 0x1000, 
		"ar-JO": 0x2C01, "ar-KW": 0x3401, "ar-LB": 0x3001, "ar-LY": 0x1001, "ar-MR": 0x1000, "ar-MA": 0x1801, 
		"ar-OM": 0x2001, "ar-PS": 0x1000, "ar-QA": 0x4001, "ar-SA": 0x0401, "ar-SO": 0x1000, "ar-SS": 0x1000, 
		"ar-SD": 0x1000, "ar-SY": 0x2801, "ar-TN": 0x1C01, "ar-AE": 0x3801, "ar-001": 0x1000, "ar-YE": 0x2401, 
		"hy": 0x002B, "hy-AM": 0x042B, "as": 0x004D, "as-IN": 0x044D, "ast": 0x1000, "ast-ES": 0x1000, 
		"asa": 0x1000, "asa-TZ": 0x1000, "az-Cyrl": 0x742C, "az-Cyrl-AZ": 0x082C, "az": 0x002C, "az-Latn": 0x782C, 
		"az-Latn-AZ": 0x042C, "ksf": 0x1000, "ksf-CM": 0x1000, "bm": 0x1000, "bm-Latn-ML": 0x1000, "bn": 0x0045, 
		"bn-BD": 0x0845, "bn-IN": 0x0445, "bas": 0x1000, "bas-CM": 0x1000, "ba": 0x006D, "ba-RU": 0x046D, 
		"eu": 0x002D, "eu-ES": 0x042D, "be": 0x0023, "be-BY": 0x0423, "bem": 0x1000, "bem-ZM": 0x1000, 
		"bez": 0x1000, "bez-TZ": 0x1000, "byn": 0x1000, "byn-ER": 0x1000, "brx": 0x1000, "brx-IN": 0x1000, 
		"bs-Cyrl": 0x641A, "bs-Cyrl-BA": 0x201A, "bs-Latn": 0x681A, "bs": 0x781A, "bs-Latn-BA": 0x141A, "br": 0x007E, 
		"br-FR": 0x047E, "bg": 0x0002, "bg-BG": 0x0402, "my": 0x0055, "my-MM": 0x0455, "ca": 0x0003, 
		"ca-AD": 0x1000, "ca-FR": 0x1000, "ca-IT": 0x1000, "ca-ES": 0x0403, "ceb": 0x1000, "ceb-Latn": 0x1000, 
		"ceb-Latn-PH": 0x1000, "tzm-Latn-MA": 0x1000, "ku": 0x0092, "ku-Arab": 0x7c92, "ku-Arab-IQ": 0x0492, "ccp": 0x1000, 
		"ccp-Cakm": 0x1000, "ccp-Cakm-BD": 0x1000, "ccp-Cakm-IN": 0x1000, "cd-RU": 0x1000, "chr": 0x005C, "chr-Cher": 0x7c5C, 
		"chr-Cher-US": 0x045C, "cgg": 0x1000, "cgg-UG": 0x1000, "zh-Hans": 0x0004, "zh": 0x7804, "zh-CN": 0x0804, 
		"zh-SG": 0x1004, "zh-Hant": 0x7C04, "zh-HK": 0x0C04, "zh-MO": 0x1404, "zh-TW": 0x0404, "cu-RU": 0x1000, 
		"swc": 0x1000, "swc-CD": 0x1000, "kw": 0x1000, "kw-GB": 0x1000, "co": 0x0083, "co-FR": 0x0483, 
		"hr,": 0x001A, "hr-HR": 0x041A, "hr-BA": 0x101A, "cs": 0x0005, "cs-CZ": 0x0405, "da": 0x0006, 
		"da-DK": 0x0406, "da-GL": 0x1000, "prs": 0x008C, "prs-AF": 0x048C, "dv": 0x0065, "dv-MV": 0x0465, 
		"dua": 0x1000, "dua-CM": 0x1000, "nl": 0x0013, "nl-AW": 0x1000, "nl-BE": 0x0813, "nl-BQ": 0x1000, 
		"nl-CW": 0x1000, "nl-NL": 0x0413, "nl-SX": 0x1000, "nl-SR": 0x1000, "dz": 0x1000, "dz-BT": 0x0C51, 
		"ebu": 0x1000, "ebu-KE": 0x1000, "en": 0x0009, "en-AS": 0x1000, "en-AI": 0x1000, "en-AG": 0x1000, 
		"en-AU": 0x0C09, "en-AT": 0x1000, "en-BS": 0x1000, "en-BB": 0x1000, "en-BE": 0x1000, "en-BZ": 0x2809, 
		"en-BM": 0x1000, "en-BW": 0x1000, "en-IO": 0x1000, "en-VG": 0x1000, "en-BI": 0x1000, "en-CM": 0x1000, 
		"en-CA": 0x1009, "en-029": 0x2409, "en-KY": 0x1000, "en-CX": 0x1000, "en-CC": 0x1000, "en-CK": 0x1000, 
		"en-CY": 0x1000, "en-DK": 0x1000, "en-DM": 0x1000, "en-ER": 0x1000, "en-150": 0x1000, "en-FK": 0x1000, 
		"en-FI": 0x1000, "en-FJ": 0x1000, "en-GM": 0x1000, "en-DE": 0x1000, "en-GH": 0x1000, "en-GI": 0x1000, 
		"en-GD": 0x1000, "en-GU": 0x1000, "en-GG": 0x1000, "en-GY": 0x1000, "en-HK": 0x3C09, "en-IN": 0x4009, 
		"en-IE": 0x1809, "en-IM": 0x1000, "en-IL": 0x1000, "en-JM": 0x2009, "en-JE": 0x1000, "en-KE": 0x1000, 
		"en-KI": 0x1000, "en-LS": 0x1000, "en-LR": 0x1000, "en-MO": 0x1000, "en-MG": 0x1000, "en-MW": 0x1000, 
		"en-MY": 0x4409, "en-MT": 0x1000, "en-MH": 0x1000, "en-MU": 0x1000, "en-FM": 0x1000, "en-MS": 0x1000, 
		"en-NA": 0x1000, "en-NR": 0x1000, "en-NL": 0x1000, "en-NZ": 0x1409, "en-NG": 0x1000, "en-NU": 0x1000, 
		"en-NF": 0x1000, "en-MP": 0x1000, "en-PK": 0x1000, "en-PW": 0x1000, "en-PG": 0x1000, "en-PN": 0x1000, 
		"en-PR": 0x1000, "en-PH": 0x3409, "en-RW": 0x1000, "en-KN": 0x1000, "en-LC": 0x1000, "en-VC": 0x1000, 
		"en-WS": 0x1000, "en-SC": 0x1000, "en-SL": 0x1000, "en-SG": 0x4809, "en-SX": 0x1000, "en-SI": 0x1000, 
		"en-SB": 0x1000, "en-ZA": 0x1C09, "en-SS": 0x1000, "en-SH": 0x1000, "en-SD": 0x1000, "en-SZ": 0x1000, 
		"en-SE": 0x1000, "en-CH": 0x1000, "en-TZ": 0x1000, "en-TK": 0x1000, "en-TO": 0x1000, "en-TT": 0x2c09, 
		"en-TC": 0x1000, "en-TV": 0x1000, "en-UG": 0x1000, "en-AE": 0x4C09, "en-GB": 0x0809, "en-US": 0x0409, 
		"en-UM": 0x1000, "en-VI": 0x1000, "en-VU": 0x1000, "en-001": 0x1000, "en-ZM": 0x1000, "en-ZW": 0x3009, 
		"eo": 0x1000, "eo-001": 0x1000, "et": 0x0025, "et-EE": 0x0425, "ee": 0x1000, "ee-GH": 0x1000, 
		"ee-TG": 0x1000, "ewo": 0x1000, "ewo-CM": 0x1000, "fo": 0x0038, "fo-DK": 0x1000, "fo-FO": 0x0438, 
		"fil": 0x0064, "fil-PH": 0x0464, "fi": 0x000B, "fi-FI": 0x040B, "fr": 0x000C, "fr-DZ": 0x1000, 
		"fr-BE": 0x080C, "fr-BJ": 0x1000, "fr-BF": 0x1000, "fr-BI": 0x1000, "fr-CM": 0x2c0C, "fr-CA": 0x0c0C, 
		"fr-CF": 0x1000, "fr-TD": 0x1000, "fr-KM": 0x1000, "fr-CG": 0x1000, "fr-CD": 0x240C, "fr-CI": 0x300C, 
		"fr-DJ": 0x1000, "fr-GQ": 0x1000, "fr-FR": 0x040C, "fr-GF": 0x1000, "fr-PF": 0x1000, "fr-GA": 0x1000, 
		"fr-GP": 0x1000, "fr-GN": 0x1000, "fr-HT": 0x3c0C, "fr-LU": 0x140C, "fr-MG": 0x1000, "fr-ML": 0x340C, 
		"fr-MQ": 0x1000, "fr-MR": 0x1000, "fr-MU": 0x1000, "fr-YT": 0x1000, "fr-MA": 0x380C, "fr-NC": 0x1000, 
		"fr-NE": 0x1000, "fr-MC": 0x180C, "fr-RE": 0x200C, "fr-RW": 0x1000, "fr-BL": 0x1000, "fr-MF": 0x1000, 
		"fr-PM": 0x1000, "fr-SN": 0x280C, "fr-SC": 0x1000, "fr-CH": 0x100C, "fr-SY": 0x1000, "fr-TG": 0x1000, 
		"fr-TN": 0x1000, "fr-VU": 0x1000, "fr-WF": 0x1000, "fy": 0x0062, "fy-NL": 0x0462, "fur": 0x1000, 
		"fur-IT": 0x1000, "ff": 0x0067, "ff-Latn": 0x7C67, "ff-Latn-BF": 0x1000, "ff-CM": 0x1000, "ff-Latn-CM": 0x1000, 
		"ff-Latn-GM": 0x1000, "ff-Latn-GH": 0x1000, "ff-GN": 0x1000, "ff-Latn-GN": 0x1000, "ff-Latn-GW": 0x1000, "ff-Latn-LR": 0x1000, 
		"ff-MR": 0x1000, "ff-Latn-MR": 0x1000, "ff-Latn-NE": 0x1000, "ff-NG": 0x1000, "ff-Latn-NG": 0x1000, "ff-Latn-SN": 0x0867, 
		"ff-Latn-SL": 0x1000, "gl": 0x0056, "gl-ES": 0x0456, "lg": 0x1000, "lg-UG": 0x1000, "ka": 0x0037, 
		"ka-GE": 0x0437, "de": 0x0007, "de-AT": 0x0C07, "de-BE": 0x1000, "de-DE": 0x0407, "de-IT": 0x1000, 
		"de-LI": 0x1407, "de-LU": 0x1007, "de-CH": 0x0807, "el": 0x0008, "el-CY": 0x1000, "el-GR": 0x0408, 
		"kl": 0x006F, "kl-GL": 0x046F, "gn": 0x0074, "gn-PY": 0x0474, "gu": 0x0047, "gu-IN": 0x0447, 
		"guz": 0x1000, "guz-KE": 0x1000, "ha": 0x0068, "ha-Latn": 0x7C68, "ha-Latn-GH": 0x1000, "ha-Latn-NE": 0x1000, 
		"ha-Latn-NG": 0x0468, "haw": 0x0075, "haw-US": 0x0475, "he": 0x000D, "he-IL": 0x040D, "hi": 0x0039, 
		"hi-IN": 0x0439, "hu": 0x000E, "hu-HU": 0x040E, "is": 0x000F, "is-IS": 0x040F, "ig": 0x0070, 
		"ig-NG": 0x0470, "id": 0x0021, "id-ID": 0x0421, "ia": 0x1000, "ia-FR": 0x1000, "ia-001": 0x1000, 
		"iu": 0x005D, "iu-Latn": 0x7C5D, "iu-Latn-CA": 0x085D, "iu-Cans": 0x785D, "iu-Cans-CA": 0x045d, "ga": 0x003C, 
		"ga-IE": 0x083C, "it": 0x0010, "it-IT": 0x0410, "it-SM": 0x1000, "it-CH": 0x0810, "it-VA": 0x1000, 
		"ja": 0x0011, "ja-JP": 0x0411, "jv": 0x1000, "jv-Latn": 0x1000, "jv-Latn-ID": 0x1000, "dyo": 0x1000, 
		"dyo-SN": 0x1000, "kea": 0x1000, "kea-CV": 0x1000, "kab": 0x1000, "kab-DZ": 0x1000, "kkj": 0x1000, 
		"kkj-CM": 0x1000, "kln": 0x1000, "kln-KE": 0x1000, "kam": 0x1000, "kam-KE": 0x1000, "kn": 0x004B, 
		"kn-IN": 0x044B, "ks": 0x0060, "ks-Arab": 0x0460, "ks-Arab-IN": 0x1000, "kk": 0x003F, "kk-KZ": 0x043F, 
		"km": 0x0053, "km-KH": 0x0453, "quc": 0x0086, "quc-Latn-GT": 0x0486, "ki": 0x1000, "ki-KE": 0x1000, 
		"rw": 0x0087, "rw-RW": 0x0487, "sw": 0x0041, "sw-KE": 0x0441, "sw-TZ": 0x1000, "sw-UG": 0x1000, 
		"kok": 0x0057, "kok-IN": 0x0457, "ko": 0x0012, "ko-KR": 0x0412, "ko-KP": 0x1000, "khq": 0x1000, 
		"khq-ML": 0x1000, "ses": 0x1000, "ses-ML": 0x1000, "nmg": 0x1000, "nmg-CM": 0x1000, "ky": 0x0040, 
		"ky-KG": 0x0440, "ku-Arab-IR": 0x1000, "lkt": 0x1000, "lkt-US": 0x1000, "lag": 0x1000, "lag-TZ": 0x1000, 
		"lo": 0x0054, "lo-LA": 0x0454, "lv": 0x0026, "lv-LV": 0x0426, "ln": 0x1000, "ln-AO": 0x1000, 
		"ln-CF": 0x1000, "ln-CG": 0x1000, "ln-CD": 0x1000, "lt": 0x0027, "lt-LT": 0x0427, "nds": 0x1000, 
		"nds-DE": 0x1000, "nds-NL": 0x1000, "dsb": 0x7C2E, "dsb-DE": 0x082E, "lu": 0x1000, "lu-CD": 0x1000, 
		"luo": 0x1000, "luo-KE": 0x1000, "lb": 0x006E, "lb-LU": 0x046E, "luy": 0x1000, "luy-KE": 0x1000, 
		"mk": 0x002F, "mk-MK": 0x042F, "jmc": 0x1000, "jmc-TZ": 0x1000, "mgh": 0x1000, "mgh-MZ": 0x1000, 
		"kde": 0x1000, "kde-TZ": 0x1000, "mg": 0x1000, "mg-MG": 0x1000, "ms": 0x003E, "ms-BN": 0x083E, 
		"ms-MY": 0x043E, "ml": 0x004C, "ml-IN": 0x044C, "mt": 0x003A, "mt-MT": 0x043A, "gv": 0x1000, 
		"gv-IM": 0x1000, "mi": 0x0081, "mi-NZ": 0x0481, "arn": 0x007A, "arn-CL": 0x047A, "mr": 0x004E, 
		"mr-IN": 0x044E, "mas": 0x1000, "mas-KE": 0x1000, "mas-TZ": 0x1000, "mzn-IR": 0x1000, "mer": 0x1000, 
		"mer-KE": 0x1000, "mgo": 0x1000, "mgo-CM": 0x1000, "moh": 0x007C, "moh-CA": 0x047C, "mn": 0x0050, 
		"mn-Cyrl": 0x7850, "mn-MN": 0x0450, "mn-Mong": 0x7C50, "mn-Mong-CN": 0x0850, "mn-Mong-MN": 0x0C50, "mfe": 0x1000, 
		"mfe-MU": 0x1000, "mua": 0x1000, "mua-CM": 0x1000, "nqo": 0x1000, "nqo-GN": 0x1000, "naq": 0x1000, 
		"naq-NA": 0x1000, "ne": 0x0061, "ne-IN": 0x0861, "ne-NP": 0x0461, "nnh": 0x1000, "nnh-CM": 0x1000, 
		"jgo": 0x1000, "jgo-CM": 0x1000, "lrc-IQ": 0x1000, "lrc-IR": 0x1000, "nd": 0x1000, "nd-ZW": 0x1000, 
		"no": 0x0014, "nb": 0x7C14, "nb-NO": 0x0414, "nn": 0x7814, "nn-NO": 0x0814, "nb-SJ": 0x1000, 
		"nus": 0x1000, "nus-SD": 0x1000, "nus-SS": 0x1000, "nyn": 0x1000, "nyn-UG": 0x1000, "oc": 0x0082, 
		"oc-FR": 0x0482, "or": 0x0048, "or-IN": 0x0448, "om": 0x0072, "om-ET": 0x0472, "om-KE": 0x1000, 
		"os": 0x1000, "os-GE": 0x1000, "os-RU": 0x1000, "ps": 0x0063, "ps-AF": 0x0463, "ps-PK": 0x1000, 
		"fa": 0x0029, "fa-AF": 0x1000, "fa-IR": 0x0429, "pl": 0x0015, "pl-PL": 0x0415, "pt": 0x0016, 
		"pt-AO": 0x1000, "pt-BR": 0x0416, "pt-CV": 0x1000, "pt-GQ": 0x1000, "pt-GW": 0x1000, "pt-LU": 0x1000, 
		"pt-MO": 0x1000, "pt-MZ": 0x1000, "pt-PT": 0x0816, "pt-ST": 0x1000, "pt-CH": 0x1000, "pt-TL": 0x1000, 
		"prg-001": 0x1000, "qps-ploca": 0x05FE, "qps-ploc": 0x0501, "qps-plocm": 0x09FF, "pa": 0x0046, "pa-Arab": 0x7C46, 
		"pa-IN": 0x0446, "pa-Arab-PK": 0x0846, "quz": 0x006B, "quz-BO": 0x046B, "quz-EC": 0x086B, "quz-PE": 0x0C6B, 
		"ksh": 0x1000, "ksh-DE": 0x1000, "ro": 0x0018, "ro-MD": 0x0818, "ro-RO": 0x0418, "rm": 0x0017, 
		"rm-CH": 0x0417, "rof": 0x1000, "rof-TZ": 0x1000, "rn": 0x1000, "rn-BI": 0x1000, "ru": 0x0019, 
		"ru-BY": 0x1000, "ru-KZ": 0x1000, "ru-KG": 0x1000, "ru-MD": 0x0819, "ru-RU": 0x0419, "ru-UA": 0x1000, 
		"rwk": 0x1000, "rwk-TZ": 0x1000, "ssy": 0x1000, "ssy-ER": 0x1000, "sah": 0x0085, "sah-RU": 0x0485, 
		"saq": 0x1000, "saq-KE": 0x1000, "smn": 0x703B, "smn-FI": 0x243B, "smj": 0x7C3B, "smj-NO": 0x103B, 
		"smj-SE": 0x143B, "se": 0x003B, "se-FI": 0x0C3B, "se-NO": 0x043B, "se-SE": 0x083B, "sms": 0x743B, 
		"sms-FI": 0x203B, "sma": 0x783B, "sma-NO": 0x183B, "sma-SE": 0x1C3B, "sg": 0x1000, "sg-CF": 0x1000, 
		"sbp": 0x1000, "sbp-TZ": 0x1000, "sa": 0x004F, "sa-IN": 0x044F, "gd": 0x0091, "gd-GB": 0x0491, 
		"seh": 0x1000, "seh-MZ": 0x1000, "sr-Cyrl": 0x6C1A, "sr-Cyrl-BA": 0x1C1A, "sr-Cyrl-ME": 0x301A, "sr-Cyrl-RS": 0x281A, 
		"sr-Cyrl-CS": 0x0C1A, "sr-Latn": 0x701A, "sr": 0x7C1A, "sr-Latn-BA": 0x181A, "sr-Latn-ME": 0x2c1A, "sr-Latn-RS": 0x241A, 
		"sr-Latn-CS": 0x081A, "nso": 0x006C, "nso-ZA": 0x046C, "tn": 0x0032, "tn-BW": 0x0832, "tn-ZA": 0x0432, 
		"ksb": 0x1000, "ksb-TZ": 0x1000, "sn": 0x1000, "sn-Latn": 0x1000, "sn-Latn-ZW": 0x1000, "sd": 0x0059, 
		"sd-Arab": 0x7C59, "sd-Arab-PK": 0x0859, "si": 0x005B, "si-LK": 0x045B, "sk": 0x001B, "sk-SK": 0x041B, 
		"sl": 0x0024, "sl-SI": 0x0424, "xog": 0x1000, "xog-UG": 0x1000, "so": 0x0077, "so-DJ": 0x1000, 
		"so-ET": 0x1000, "so-KE": 0x1000, "so-SO": 0x0477, "st": 0x0030, "st-ZA": 0x0430, "nr": 0x1000, 
		"nr-ZA": 0x1000, "st-LS": 0x1000, "es": 0x000A, "es-AR": 0x2C0A, "es-BZ": 0x1000, "es-VE": 0x200A, 
		"es-BO": 0x400A, "es-BR": 0x1000, "es-CL": 0x340A, "es-CO": 0x240A, "es-CR": 0x140A, "es-CU": 0x5c0A, 
		"es-DO": 0x1c0A, "es-EC": 0x300A, "es-SV": 0x440A, "es-GQ": 0x1000, "es-GT": 0x100A, "es-HN": 0x480A, 
		"es-419": 0x580A, "es-MX": 0x080A, "es-NI": 0x4C0A, "es-PA": 0x180A, "es-PY": 0x3C0A, "es-PE": 0x280A, 
		"es-PH": 0x1000, "es-PR": 0x500A, "es-ES_tradnl": 0x040A, "es-ES": 0x0c0A, "es-US": 0x540A, "es-UY": 0x380A, 
		"zgh": 0x1000, "zgh-Tfng-MA": 0x1000, "zgh-Tfng": 0x1000, "ss": 0x1000, "ss-ZA": 0x1000, "ss-SZ": 0x1000, 
		"sv": 0x001D, "sv-AX": 0x1000, "sv-FI": 0x081D, "sv-SE": 0x041D, "syr": 0x005A, "syr-SY": 0x045A, 
		"shi": 0x1000, "shi-Tfng": 0x1000, "shi-Tfng-MA": 0x1000, "shi-Latn": 0x1000, "shi-Latn-MA": 0x1000, "dav": 0x1000, 
		"dav-KE": 0x1000, "tg": 0x0028, "tg-Cyrl": 0x7C28, "tg-Cyrl-TJ": 0x0428, "tzm": 0x005F, "tzm-Latn": 0x7C5F, 
		"tzm-Latn-DZ": 0x085F, "ta": 0x0049, "ta-IN": 0x0449, "ta-MY": 0x1000, "ta-SG": 0x1000, "ta-LK": 0x0849, 
		"twq": 0x1000, "twq-NE": 0x1000, "tt": 0x0044, "tt-RU": 0x0444, "te": 0x004A, "te-IN": 0x044A, 
		"teo": 0x1000, "teo-KE": 0x1000, "teo-UG": 0x1000, "th": 0x001E, "th-TH": 0x041E, "bo": 0x0051, 
		"bo-IN": 0x1000, "bo-CN": 0x0451, "tig": 0x1000, "tig-ER": 0x1000, "ti": 0x0073, "ti-ER": 0x0873, 
		"ti-ET": 0x0473, "to": 0x1000, "to-TO": 0x1000, "ts": 0x0031, "ts-ZA": 0x0431, "tr": 0x001F, 
		"tr-CY": 0x1000, "tr-TR": 0x041F, "tk": 0x0042, "tk-TM": 0x0442, "uk": 0x0022, "uk-UA": 0x0422, 
		"hsb": 0x002E, "hsb-DE": 0x042E, "ur": 0x0020, "ur-IN": 0x0820, "ur-PK": 0x0420, "ug": 0x0080, 
		"ug-CN": 0x0480, "uz-Arab": 0x1000, "uz-Arab-AF": 0x1000, "uz-Cyrl": 0x7843, "uz-Cyrl-UZ": 0x0843, "uz": 0x0043, 
		"uz-Latn": 0x7C43, "uz-Latn-UZ": 0x0443, "vai": 0x1000, "vai-Vaii": 0x1000, "vai-Vaii-LR": 0x1000, "vai-Latn-LR": 0x1000, 
		"vai-Latn": 0x1000, "ca-ES-valencia": 0x0803, "ve": 0x0033, "ve-ZA": 0x0433, "vi": 0x002A, "vi-VN": 0x042A, 
		"vo": 0x1000, "vo-001": 0x1000, "vun": 0x1000, "vun-TZ": 0x1000, "wae": 0x1000, "wae-CH": 0x1000, 
		"cy": 0x0052, "cy-GB": 0x0452, "wal": 0x1000, "wal-ET": 0x1000, "wo": 0x0088, "wo-SN": 0x0488, 
		"xh": 0x0034, "xh-ZA": 0x0434, "yav": 0x1000, "yav-CM": 0x1000, "ii": 0x0078, "ii-CN": 0x0478, 
		"yo": 0x006A, "yo-BJ": 0x1000, "yo-NG": 0x046A, "dje": 0x1000, "dje-NE": 0x1000, "zu": 0x0035, 
		"zu-ZA": 0x0435];


	    wchar[255] buf;
		auto p = &buf[0];
    	auto size = cast(int)buf.length - 1;

	    auto LOCALE_SNAME = 0x0000005c;
        GetLocaleInfo(LOCALE_USER_DEFAULT, LOCALE_SNAME, p, size);

        setDefaultLocale(to!string(p.fromStringz));
    }
}

version(Windows)
{
    private string discover(LCID lcid, LCTYPE type)
    {
        wchar[255] buf;
		auto p = &buf[0];
    	auto size = cast(int)buf.length - 1;

        GetLocaleInfoW(lcid, type, p, size);
        return to!string(p.fromStringz);
    }

    /// Converts MS date-time format style to strftime, that library uses
    private string getAndConvertFormat(LCID lcid, LCTYPE type)
    {
        // Get parameter from Windows
        return convertMs2posixFormat(discover(lcid, type));
    }
        
    /// Convert Windows style format to Unix
    private string convertMs2posixFormat(string source)
    {
        string makeConvert(string source, char symbol, string[] subst, out uint c)
        {
            auto part = source[ 1 .. min(subst.length, source.length) ];
            c = 0;
            while (part.length > 0)
            {
                if (part[0] != symbol)
                    break;
                c++;
                part = part[ 1 .. $ ];
            }
			auto result = subst[c];
			c++;
            return result;
        }

        char[] target;
		
		while (source.length > 0)
        {
            auto ch = source[0];
            string part;
			uint c;
            switch (ch)
            {
                case 'g':
                case 't':
					source = source[ 1 .. $ ];
                    break;
                case 'd':
                    part = makeConvert(source, ch, ["%d", "%d", "%a", "%A"], c);
                    target ~= part;
                    source = source[ c .. $ ];
                    break;
                case 'M':
                    part = makeConvert(source, ch, ["%m", "%m", "%b", "%B"], c);
                    target ~= part;
                    source = source[ c .. $ ];
                    break;
                case 'y':
                    part = makeConvert(source, ch, ["%y", "%y", "yyy", "%Y", "%Y"], c);
                    target ~= part;
                    source = source[ c .. $ ];
                    break;
                case 'h':
                    part = makeConvert(source, ch, ["%I", "%I"], c);
                    target ~= part;
                    source = source[ c .. $ ];
                    break;
                case 'H':
                    part = makeConvert(source, ch, ["%H", "%H"], c);
                    target ~= part;
                    source = source[ c .. $ ];
                    break;
                case 'm':
                    part = makeConvert(source, ch, ["%M", "%M"], c);
                    target ~= part;
                    source = source[ c .. $ ];
                    break;
                case 's':
                    part = makeConvert(source, ch, ["%S", "%S"], c);
                    target ~= part;
                    source = source[ c .. $ ];
                    break;
                default:
                    source = source[ 1 .. $ ];
                    target ~= ch;
            }
        }

        return to!string(target).strip;
    }
}

// Unittest for supported systems.
unittest
{
    import std.stdio;
    writeln("Default system locale is " ~ defaultLocale.name);

    Locale locale;
    version(Posix)
        locale = initDateformatLocale("C");
    else
        version(Windows)
            locale = initDateformatLocale("en");
        else
            locale = initDateformatLocale("en-US");
    setDefaultLocale(locale.name);
    writeln("Test's locale is " ~ locale.name);

    assert(locale.weekDays[0] == "Sunday");
    assert(locale.weekDays[1] == "Monday");
    assert(locale.weekDays[2] == "Tuesday");
    assert(locale.weekDays[3] == "Wednesday");
    assert(locale.weekDays[4] == "Thursday");
    assert(locale.weekDays[5] == "Friday");
    assert(locale.weekDays[6] == "Saturday");
    
    assert(locale.abbrWeekDays[0] == "Sun");
    assert(locale.abbrWeekDays[1] == "Mon");
    assert(locale.abbrWeekDays[2] == "Tue");
    assert(locale.abbrWeekDays[3] == "Wed");
    assert(locale.abbrWeekDays[4] == "Thu");
    assert(locale.abbrWeekDays[5] == "Fri");
    assert(locale.abbrWeekDays[6] == "Sat");

    assert(locale.monthes[0] == "January");
    assert(locale.monthes[1] == "February");
    assert(locale.monthes[2] == "March");
    assert(locale.monthes[3] == "April");
    assert(locale.monthes[4] == "May");
    assert(locale.monthes[5] == "June");
    assert(locale.monthes[6] == "July");
    assert(locale.monthes[7] == "August");
    assert(locale.monthes[8] == "September");
    assert(locale.monthes[9] == "October");
    assert(locale.monthes[10] == "November");
    assert(locale.monthes[11] == "December");

    assert(locale.abbrMonthes[0] == "Jan");
    assert(locale.abbrMonthes[1] == "Feb");
    assert(locale.abbrMonthes[2] == "Mar");
    assert(locale.abbrMonthes[3] == "Apr");
    assert(locale.abbrMonthes[4] == "May");
    assert(locale.abbrMonthes[5] == "Jun");
    assert(locale.abbrMonthes[6] == "Jul");
    assert(locale.abbrMonthes[7] == "Aug");
    assert(locale.abbrMonthes[8] == "Sep");
    assert(locale.abbrMonthes[9] == "Oct");
    assert(locale.abbrMonthes[10] == "Nov");
    assert(locale.abbrMonthes[11] == "Dec");
    
    assert(locale.am == "AM");
    assert(locale.pm == "PM");

    version(Posix)
    {
        writeln("Posix tests");

	    assert(locale.codeSet == "ANSI_X3.4-1968");
	    assert(locale.era == "");
	    assert(locale.dateTime !is null && locale.dateTime.length > 2);
	    assert(locale.date !is null && locale.date.length > 2);
	    assert(locale.time !is null && locale.time.length > 2);
	
        assert(locale.decimalPoint == ".");
	    assert(locale.thousandSeparator == "");
	
	    assert(locale.currency == "");
	    assert(locale.currencyNegativePlace == 1);
	    assert(locale.currencyPositivePlace == 1);
	    assert(locale.intCurrency == "");
	    assert(locale.intCurrencyNegativePlace == 1);
	    assert(locale.intCurrencyPositivePlace == 1);
        
        assert(locale.fracDigits == 0);
        assert(locale.intFracDigits == 0);
    }

    version(Windows)
    {
        writeln("Windows tests");

        assert(locale.codeSet == "437");
	    assert(locale.era == "");
	    assert(locale.dateTime !is null && locale.dateTime.length > 2);
	    assert(locale.date !is null && locale.date.length > 2);
	    assert(locale.time !is null && locale.time.length > 2);
	
        assert(locale.decimalPoint == ".");
	    assert(locale.thousandSeparator == ",");
	
	    assert(locale.currency == "$");
	    assert(locale.currencyNegativePlace == 1);
	    assert(locale.currencyPositivePlace == 1);
	    assert(locale.intCurrency == "$");
	    assert(locale.intCurrencyNegativePlace == 1);
	    assert(locale.intCurrencyPositivePlace == 1);
        
        assert(locale.fracDigits == 2);
        assert(locale.intFracDigits == 2);

        assert(convertMs2posixFormat("d") == "%d");
        assert(convertMs2posixFormat("dd") == "%d");
        assert(convertMs2posixFormat("ddd") == "%a");
        assert(convertMs2posixFormat("dddd") == "%A");

        assert(convertMs2posixFormat("M") == "%m");
        assert(convertMs2posixFormat("MM") == "%m");
        assert(convertMs2posixFormat("MMM") == "%b");
        assert(convertMs2posixFormat("MMMM") == "%B");

        assert(convertMs2posixFormat("y") == "%y");
        assert(convertMs2posixFormat("yy") == "%y");
        assert(convertMs2posixFormat("yyyy") == "%Y");
        assert(convertMs2posixFormat("yyyyy") == "%Y");

        assert(convertMs2posixFormat("g") == "");
        assert(convertMs2posixFormat("gg") == "");

        assert(convertMs2posixFormat("dd.MM.yyyy g") == "%d.%m.%Y");
    }
}
