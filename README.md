# Locale Management Library for D

Library collects locale info for Posix (tested on Linux) and for Windows ( >= 7 ) into **Locale** structure. Use **initDateformatLocale** function to get locale info. Name for locale differs on various platforms. See documentation on yours.

**setDefaultLocale** sets default locale, but only useful with other libraries.
